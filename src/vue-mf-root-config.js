import { registerApplication, start } from "single-spa";

registerApplication({
  name: "@vue-mf/landingpage",
  app: () => System.import("@vue-mf/landingpage"),
  activeWhen: "/",
});

registerApplication({
  name: "@vue-mf/search",
  app: () => System.import("@vue-mf/search"),
  activeWhen: "/search",
});

registerApplication({
  name: "@vue-mf/materi",
  app: () => System.import("@vue-mf/materi"),
  activeWhen: ["/materi", "/kuis"],
});

start();
